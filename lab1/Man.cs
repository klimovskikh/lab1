﻿using System;

namespace lab1
{

    class Man
    {
        string lname;
        string fname;
        string pname;
        string phone;
        string country;
        string bdate;
        string organization;
        string position;
        string notes;

        public Man(string[] args)
        {
            this.lname = args[0];
            this.fname = args[1];
            this.pname = args[2];
            this.phone = args[3];
            this.country = args[4];
            this.bdate = args[5];
            this.organization = args[6];
            this.position = args[7];
            this.notes = args[8];

        }

        public override string ToString()
        {
            string full_return = $"1.Фамилия: {lname}\n" +
                $"2.Имя: {fname}\n" +
                $"3.Отчество: {pname}\n" +
                $"4.Телефон: {phone}\n" +
                $"5.Страна: {country}\n" +
                $"6.Дата рождения: {bdate}\n" +
                $"7.Организация: {organization}\n" +
                $"8.Должность: {position}\n" +
                $"9.Заметки: {notes}\n";

            return full_return;
        }

        public void EditField(int field, string value)
        {
            switch (field)
            {
                case 1:
                    if (value != "")
                    {
                        this.lname = value;
                        Console.WriteLine("Поле изменено!");
                    }

                    else
                    {
                        Console.WriteLine("Обязательное поле не может быть пустым!");
                    }
                    
                    break;

                case 2:
                    if (value != "")
                    {
                        this.fname = value;
                        Console.WriteLine("Поле изменено!");
                    }
                    else
                    {
                        Console.WriteLine("Обязательное поле не может быть пустым!");
                    }

                    break;

                case 3:
                    this.pname = value;
                    Console.WriteLine("Поле изменено!");
                    break;

                case 4:
                    if (value != "")
                    {
                        this.phone = value;
                        Console.WriteLine("Поле изменено!");
                    }
                    else
                    {
                        Console.WriteLine("Обязательное поле не может быть пустым!");
                    }

                    break;

                case 5:
                    if (value != "")
                    {
                        this.country = value;
                        Console.WriteLine("Поле изменено!");
                    }
                    else
                    {
                        Console.WriteLine("Обязательное поле не может быть пустым!");
                    }

                    break;

                case 6:
                    this.bdate = value;
                    Console.WriteLine("Поле изменено!");
                    break;

                case 7:
                    this.organization = value;
                    Console.WriteLine("Поле изменено!");
                    break;

                case 8:
                    this.position = value;
                    Console.WriteLine("Поле изменено!");
                    break;

                case 9:
                    this.notes = value;
                    Console.WriteLine("Поле изменено!");
                    break;

                default:
                    Console.WriteLine("Неверный номер поля");
                    break;
            }

        }

    }
}