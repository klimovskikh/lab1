﻿using System;
using System.Collections;
using System.Linq;

namespace lab1
{
    class Program
    {
        public static ArrayList people_list = new ArrayList(); //для хранения всех записей

        static void Main(string[] args)
        {
            
            Console.WriteLine("Записная книга");
            while (true){
                Console.WriteLine("Нажмите:\n" +
                    "1 для добавления контакта\n" +
                    "2 для просмотра всех контактов\n" +
                    "3 для редактирования контакта\n" +
                    "4 для удаления контакта\n" +
                    "5 для выхода");

                string user_choice = Console.ReadLine();

                if (user_choice == "1")
                {
                    AddMan();
                }

                else if (user_choice == "2")
                {
                    ShowAll();
                }

                else if (user_choice == "3")
                {
                    EditMan();
                }

                else if (user_choice == "4")
                {
                    DropMan();
                }

                else if (user_choice == "5")
                {
                    Console.WriteLine("До встречи!");
                    break;
                }

            }
            
        }

        static void AddMan()
        {
            string[] new_entry = new string[9];
            string[] fields = { "Фамилия (обязательно): ", "Имя (обязательно): ",
                "Отчество: ", "Телефон (обязательно): ", "Страна (обязательно): ",
                "Дата рождения: ", "Организация: ", "Должность: ", "Заметки: "};

            Console.Clear();
            Console.WriteLine("Добавление нового контакта");
            for (int i = 0; i < fields.Length; i++)
            {
                Console.Write(fields[i]);
                new_entry[i] = Console.ReadLine();
            }

            if (new_entry[0] != "" && new_entry[1] != "" && new_entry[3] != ""
                && new_entry[4] != "" && (new_entry[3]).All(char.IsDigit))
            {
                people_list.Add(new Man(new_entry));
                Console.Clear();
                Console.WriteLine("Запись добавлена!\n");
            }

            else
            {
                Console.WriteLine("Одно из обязательных полей пусто или заполнено " +
                    "в неверном формате. " +
                   "Запись не была добавлена");
            }


        }


        static void ShowAll()
        {
            Console.Clear();
            if (people_list.Count != 0)
            {
                Console.WriteLine("Просмотр всех контактов");
                for (int i = 0; i < people_list.Count; i++)
                {
                    Console.WriteLine($"Контакт №{i + 1}");
                    Console.WriteLine(people_list[i]);
                }
                Console.WriteLine("Список завершен!\n");
            }

            else
            {
                Console.WriteLine("В записной книге пусто!");
            }
            
        }

        static void EditMan()
        {
            Console.Clear();
            if (people_list.Count != 0)
            {
                Console.WriteLine("Показать все контакты?\n" +
                    "Нажмите \"д\" для показа или любую кнопку для продолжения\n" +
                     "Для редактирования необходимо ввести номер контакта в " +
                       "записной книге");
                string user_drop_choice = Console.ReadLine();
                if (user_drop_choice == "д")
                {
                    ShowAll();
                }

                Console.Write("Введите номер контакта: ");

                try
                {
                    int i = Convert.ToInt32(Console.ReadLine());
                    Console.WriteLine(people_list[i - 1]);
                    Console.Write("Введите номер поля, которое хотите изменить: ");
                    int j = Convert.ToInt32(Console.ReadLine());

                    Console.Write("Введите новое значение поля: ");
                    string new_value = Console.ReadLine();

                    Man man = (lab1.Man)people_list[i - 1];
                    man.EditField(j, new_value);
                }

                catch
                {
                    Repeater("edit");
                }


            }

            else
            {
                Console.WriteLine("В записной книге пусто!");
            }

        }

        static void DropMan()
        {
            Console.Clear();
            if (people_list.Count != 0)
            {
                Console.WriteLine("Показать все контакты?\n" +
                    "Нажмите \"д\" для показа или любую кнопку для продолжения\n" +
                        "Для удаления необходимо ввести номер контакта в " +
                        "записной книге");
                string user_drop_choice = Console.ReadLine();
                if (user_drop_choice == "д")
                {
                    ShowAll();
                }

                Console.Write("Введите номер контакта: ");
                try
                {
                    people_list.RemoveAt(Convert.ToInt32(Console.ReadLine()) - 1);
                    Console.Clear();
                    Console.WriteLine("Контакт удален!");
                }

                catch 
                {
                    Repeater("drop"); 
                }
            }

            else
            {
                Console.WriteLine("В записной книге пусто!");
            }


        }

        static void Repeater(string arg) //один метод вместо двух одинаковых блоков
        {
            Console.WriteLine("Контакт с таким номером не найден!");
            Console.WriteLine("Повторить или выйти?\n" +
                "Нажмите \"п\" для повтора или любую кнопку для выхода\n");
            string user_drop_choice_repeat = Console.ReadLine();
            if (user_drop_choice_repeat == "п")
            {
                if (arg == "edit")
                {
                    EditMan();
                }

                else if (arg == "drop")
                {
                    DropMan();
                }
            }

            Console.Clear();
        }

    }

}
